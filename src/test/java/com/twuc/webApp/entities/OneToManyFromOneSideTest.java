package com.twuc.webApp.entities;

import com.twuc.webApp.repository.StudentRepository;
import com.twuc.webApp.repository.TeacherRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class OneToManyFromOneSideTest {
    @Autowired
    TeacherRepository teacherRepository;

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    EntityManager entityManager;

    @Test
    void should_save_2_student_from_one_side() {
        Student student = new Student("LiJiahao");
        Student anotherStudent = new Student("SuperCool");
        Teacher teacher = new Teacher("DuJuan");

        teacher.addStudent(student);
        teacher.addStudent(anotherStudent);
        teacherRepository.saveAndFlush(teacher);

        entityManager.clear();

        int studentNumber = studentRepository.findAll().size();

        assertEquals(2, studentNumber);
    }
}